/*

note: registers 251+ are reserved
251 is sometimes a temp register for object code
252 for require code

*/

c=require('./comp')

var scanf = require('scanf');

var fs = require('fs');

var peg = require("pegjs");

let pass = function() {}

let print2 = console.log

//let print = pass
let print = console.log

let log = '', lvl = 0

let _ = pass, step = false, args2 = undefined



let assert = function(a, b) {
	if(a)
		0;
	else
		throw b;
}

function s_ref() {
	return 'rel#'+this.name
}

s = fs.readFileSync('parser.peg', 'utf8')

parser = peg.generate(s)

//print(parser.parse('asd or "2" or sda'))

var p=function(s) {
	let s1=parser.parse(s)
	print(';',s1)
	let f=def1(s1)
	//throw 2;
	return f
}

/*
It's all passed to registers before an external c:function (js:function?) is called.
It can be safely assumed they have been deferenced.
It's also up to the writer to see that functions that can receive a ref unbound deal with them,
if a function receives bound values only for some arguments this is to be set in other parts of the language so you can treat them as bound here.
If the ref is unbound you can use the function 'set' to set it, if it's guaranteed to be so.
*/

/* writing to file */

function buf(a) {
	return Buffer.from(a,'binary')
}

/*
let args = process.argv, name = args[2], flags = args[3] //name of file and flag
print(args)
if(name=='-b') { //breakpoint
	args2 = flags
	flags = name
	name = undefined
	//throw 'no file name given'
}
else if(name=='--bc') {
	name = flags
	//throw 'no file name given'
}

print(flags, name)
if(!undef(flags)&&flags=='--step') { //debug flag was set
	print(flags, name, args2)
	if(undef(args2))
		step = true
	else {
		
	}
}
*/

/*
fs.open('log', 'w', (err, fd) => {
	if (err) {
		throw err;
	}
	//print(fd)
	fs.write(fd, log);
});*/

var p=function(s) {
	print(';',s,typeof(s))
	let s1=parser.parse(s)
}

regs={}

let fn = {
	ws : function() {
		print('#', str(regs[0]), typeof regs[0])
		o=regs[0]
		o2=regs[1]
		unify(o2, ws(o))
		//throw 21;
	},
	lexer : function() {
		//print('#', str(regs[0]), typeof regs[0])
		o=regs[0]
		o2=regs[1]
		let l=lexer(o.value)
		print(str_full(l))
		unify(o2, l)
	},
	parser : function() {
		//print('#', str(regs[0]), typeof regs[0])
		o=regs[0]
		o2=regs[1]
		//let l=p('true and x=2')
		let l=p(o.value)//p('rel p() true and x=2;')
		print(str(l))
		//throw 1;
		unify(o2, l)
	},
	parser2 : function() {
		//print('#', str(regs[0]), typeof regs[0])
		o=regs[0]
		//let l=p('true and x=2')
		//s='if(true) true else x=1;'
		//s='a.p ( 2, 1 )'
		s=' a.p ( \'a\', 2, 1 )'
		s=" string.at ( 'a' , 0 , c )"
		//s='string.at ( 1 , 0 , c )'
		let l=p(s) //p('/*c::p() and*/ a="asd" and x/* */=2')
		print('l',str(l))
		//throw 1;
		//unify(o, l)
	}
}


function load(s) {
	try {
		data = fs.readFileSync(s, 'utf8')
	} catch(e) {
		print(e, typeof(e))
		throw 'Error: no file '+s;
		//throw e;
	}
	try {
		print(';',data)
		l=parser.parse(data)
		//l=parser.parse('x=2 or true and false or x=3')
		print(';',l.param[1],l.param[1].param[1])
	} catch(e) {
		print(typeof(e))
		//throw 'Error: no file '+s;
		throw e;
	}
	
}


//step = true
//e = new Env(load(name+'.bc'), []); e.cur = [0, 0]
//l=parser.parse('x=2 or true and false or x=3')

load('temp')
/*
fs.writeFile('log', log, (err) => {
  if (err) throw err;
});


l=parser.parse('x=2 or true and false or x=3')

//l=parser.parse('x=2')
//l=parser.parse('x=2 or true')
//l=parser.parse('p(2)')
l=p('case true case false;')

print(';',l)

//print(l.param[0])


//f = set_f(l.name, l.param[0], l.param[1])
f = def(l)
print(f)
print(str(f))
*/