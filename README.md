**make a parser**

---

## Status

[] fails on reading complex ids, i.e. 'int a=2' instead of 'a=2', see test0/temp

Not all files uploaded yet.

Problem files put on test0/temp

1. temp is a temporary file with removed whitespace-sensitive info from normal files, such as test0.co
2. this is ideally parsed by parser.peg into objects
3. this is then turned into bytecode

node comp.js test22 test0 -> temp -> .bc

Alternatively, make a bytecode compiler in ocaml/what you prefer/etc (example of the lang in .co/.cosmos files in here or github; not all features are/have to be implemented or used yet).

---
